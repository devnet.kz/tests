FROM php:8.1-cli

LABEL maintainer="Yerlen Zhubangaliyev"
LABEL email="yerlen@yerlen.com"
LABEL version="0.0.1"

WORKDIR /home/codecept

RUN apt-get update \
    && apt-get install -y wget libzip-dev zlib1g-dev \
    && CFLAGS="$CFLAGS -D_GNU_SOURCE" docker-php-ext-install sockets zip \
    && wget https://getcomposer.org/download/latest-stable/composer.phar \
    && chmod +x composer.phar \
    && mv composer.phar /usr/local/bin/composer \
    && php /usr/local/bin/composer require "codeception/codeception"

RUN useradd codecept -d /home/codecept -s /bin/bash && chown -R codecept.codecept .

USER codecept
