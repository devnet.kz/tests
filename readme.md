# Devnet.kz API tests

## Run tests

```bash
docker run -it --rm --name mycodeception -v "$(PWD)/src:/home/codecept" --env-file .env.local mycodeception/php8.1-cli:latest php vendor/bin/codecept run --steps --debug
```
