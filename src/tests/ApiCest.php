<?php

use Codeception\Scenario;

class ApiCest
{

    public function tryGetIndexAndSettings(ApiTester $I, Scenario $scenario)
    {
        $body = [
            "deep"   => true,
            "text"   => false,
            "sortBy" => [
                [
                    "createdAt" => "desc",
                ],
            ],
            "only"   => [
                "title",
                "path",
                "createdAt",
            ],
        ];

        $I->sendPost('/ru', $body);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}
