build:
	docker build -t mycodeception/php8.1-cli:latest .

run:
	docker run -it --rm --name mycodeception -v "$(PWD)/src:/home/codecept" --env-file .env.local mycodeception/php8.1-cli:latest bash
